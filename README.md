# frontend-interview-api

a [Sails v1](https://sailsjs.com) application

# Setup

```
npm i
npm start, followed by 3
```

To test out the APIs
- install Postman (API Client)
- import `Frontend Test Mock APIs.postman_collection.json` and `Frontend Test Mock APIs.postman_environment.json`
- assign the Postman environment to `Frontend Test Mock APIs` and try it out.
